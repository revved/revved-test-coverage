# Testing using Mocha and jscoverage

The tests are pointing to lib-cov directory so in order to run test themselves you need to update the references to lib-cov to lib in the tests themselves.

Tests can be run using:

    npm test

Coverage report can be generated using:

    rm -fr lib-cov
    jscoverage lib lib-cov
    mocha -R html-cov > coverage.html