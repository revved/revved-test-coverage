var assert = require('assert'),
    dbl = require('../lib-cov/double');

describe('simple function', function() {
  it('should return 4 when passed 2', function(done) {
    assert.equal(dbl(2), 4, 'Should be equal to 4');
    done();
  });
  it('should not return 4 when passed 3', function(done) {
    assert.notEqual(dbl(3), 4, 'Should not be equal to 4');
    done();
  });
});