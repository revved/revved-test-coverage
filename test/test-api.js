var should = require('should'),
    request = require('request'),
    app = require('../lib-cov/app');

var port = process.env.PORT || 3000;
var basePath = 'http://localhost:' + port;

describe('API', function(){
  describe('/hello', function(){
    it('should return Hello World', function(done){
      request(basePath + '/hello', function(err, res, body) {
        should.not.exist(err);
        body.should.equal('Hello World');
        done();
      });
    });
  });
});