var express = require('express');
var app = express();

app.get('/hello', function(req, res){
  res.send('Hello World');
});

app.get('/foo', function(req, res) {
  res.send('bar');
});

app.listen(process.env.PORT || 3000);
console.log('Listening on port 3000');